package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseSetup;
import base.Constants;
import base.ValidateUIHelpers;
import junit.framework.Assert;
import pages.HomePage;
import pages.LogInPage;
import pages.SocialNetworkPage;

public class LoginTF extends BaseSetup {

	private WebDriver driver;

	public HomePage homePage;

	public LogInPage loginPage;

	public SocialNetworkPage socialNetworkPage;

	public ValidateUIHelpers validateUIHelpers;

	@BeforeMethod
	public void setUp() {
		driver = getDriver();
		loginPage = new LogInPage(driver);
		homePage = new HomePage(driver);
		socialNetworkPage = new SocialNetworkPage(driver);
	}

	/*
	 * Id: TC001
	 * Objectives: Check login feature with Chotot account successfully.
	 */
	@Test()
	public void loginWithChototAccount() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.authenticationWithChototAccount(Constants.TELEPHONE, Constants.PASSWORD);
		Thread.sleep(1000);

		// Verify display homepage and account name
		Assert.assertEquals(Constants.URL_HOME_PAGE, driver.getCurrentUrl());
		Assert.assertEquals(Constants.USER_NAME_CHOTOT, homePage.getUserName());
	}

	/*
	 * Id: TC002
	 * Objectives: Check login feature with Facebook account successfully.
	 */
	@Test()
	public void loginWithFacebookAccount() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.clickFacebookLink();
		socialNetworkPage.authenticationWithFbAccount(Constants.EMAIL_FACEBOOK, Constants.PASSWORD_FACEBOOK);
		Thread.sleep(5000);

		// Verify display homepage and account name
		Assert.assertEquals(Constants.URL_HOME_PAGE, driver.getCurrentUrl());
		Assert.assertEquals(Constants.USER_NAME_FACEBOOK, homePage.getUserName());
	}

	/*
	 * Id: TC003
	 * Objectives: Check login feature with Google account successfully.
	 */
	@Test()
	public void loginWithGoogleAccount() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.clickGoogleLink();
		socialNetworkPage.authenticationWithGGAccount(Constants.EMAIL_GOOGLE, Constants.PASSWORD_GOOGLE);
		Thread.sleep(3000);

		// Verify display homepage and account name
		Assert.assertEquals(Constants.URL_HOME_PAGE, driver.getCurrentUrl());
		Assert.assertEquals(Constants.USER_NAME_FACEBOOK, homePage.getUserName());
	}

	/*
	 * Id: TC004
	 * Objectives: Check error message when user login with a phone number that has not been registered for an account.
	 */
	@Test()
	public void loginWithPhoneNumberNotRegisteredAccount() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.authenticationWithChototAccount(Constants.TELEPHONE_NOT_REGISTERED, Constants.PASSWORD);
		Thread.sleep(1000);

		// Verify display a error message with content “user information not found”.
		loginPage.errorIconTelephhone().isDisplayed();
		loginPage.errorMessageTelephhone().isDisplayed();
		boolean actualValue = loginPage.submitLogin().isEnabled();
		Assert.assertEquals(false, actualValue);
	}

	/*
	 * Id: TC005
	 * Objectives: Check error message when user login with a password incorrect.
	 */
	@Test()
	public void loginWithPasswordIncorrect() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.authenticationWithChototAccount(Constants.TELEPHONE, Constants.PASSWORD_INCRORRECT);
		loginPage.submitLogin().click();

		// Verify display a error message with content “incorrect phone number or password”.
		loginPage.errorMessagePassword().isDisplayed();
	}

	/*
	 * Id: TC006
	 * Objectives: Check error message when user login with a password less than 5 characters.
	 */
	@Test()
	public void loginWithPasswordLessThan5Characters() throws Exception {
		loginPage.closeSurveyPopin();
		loginPage.authenticationWithChototAccount(Constants.TELEPHONE, Constants.PASSWORD_LESS_THAN_5_CHARACTERS);
		loginPage.submitLogin().click();

		// Verify display a error message with content “incorrect phone number password”.
		loginPage.errorMessagePassword().isDisplayed();
	}
}
