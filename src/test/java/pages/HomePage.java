package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	private WebDriver driver;

	// Selector the text username acccount
	private By userNameText = By.cssSelector("div a.n1y9jvjc span.show-desktop");

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public String getUserName() {
		WebElement userNameTxt = driver.findElement(userNameText);
		String getUserNameAccount = userNameTxt.getText();
		return getUserNameAccount;
	}
}