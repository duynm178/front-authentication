package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import base.ValidateUIHelpers;

public class LogInPage {

	private WebDriver driver;

	private ValidateUIHelpers validateUIHelpers;

	// Selector the telephone number input
	private By telInput = By.cssSelector("input[type='tel']");

	// Selector the password input
	private By passwordInput = By.cssSelector("input[type='password']");

	// Selector the button submit
	private By submitBtn = By.cssSelector("button[type='submit']");

	// Selector the link login with Facebook
	private By facebookLink = By.cssSelector("li[src='https://static.chotot.com/storage/assets/LOGIN/facebook.svg']");

	// Selector the link login with Google
	private By googleLink = By.cssSelector("li[src='https://static.chotot.com/storage/assets/LOGIN/google.svg']");

	// Selector the icon error with Telephone number that has not been registered.
	private By errorIconWithTelephoneNotRegistered = By.cssSelector("span.error");

	// Selector the error message with Telephone number that has not been registered.
	private By errorMessageWithTelephoneNotRegistered = By.cssSelector("p.prswihc[color='#D0021B']");

	// Selector the error message with a password incorrect.
	private By errorMessageWithPasswordIncorrect = By.cssSelector("div.error");

	public LogInPage(WebDriver driver) {
		this.driver = driver;
		validateUIHelpers = new ValidateUIHelpers(this.driver);
	}

	public void clickFacebookLink() {
		validateUIHelpers.clickElement(facebookLink);
	}

	public void clickGoogleLink() {
		validateUIHelpers.clickElement(googleLink);
	}

	public HomePage authenticationWithChototAccount(String telephone, String password) {
		validateUIHelpers.waitForPageLoaded();
		Assert.assertTrue(validateUIHelpers.verifyElementText(submitBtn, "Đăng nhập"), "Not the login page");
		validateUIHelpers.setText(telInput, telephone);
		validateUIHelpers.setText(passwordInput, password);
		validateUIHelpers.clickElement(submitBtn);

		return new HomePage(this.driver);
	}

	public WebElement errorIconTelephhone() {
		return driver.findElement(errorIconWithTelephoneNotRegistered);
	}

	public WebElement errorMessageTelephhone() {
		return driver.findElement(errorMessageWithTelephoneNotRegistered);
	}

	public WebElement errorMessagePassword() {
		return driver.findElement(errorMessageWithPasswordIncorrect);
	}

	public WebElement submitLogin() {
		return driver.findElement(submitBtn);
	}

	public void closeSurveyPopin() {
		List<WebElement> closePopin = driver.findElements(By.cssSelector("button.ab-close-button svg"));
		for (WebElement we : closePopin) {
			we.click();
		}
	}
}
