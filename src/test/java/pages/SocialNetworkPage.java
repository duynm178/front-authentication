package pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import base.ValidateUIHelpers;

public class SocialNetworkPage {

	private WebDriver driver;

	private ValidateUIHelpers validateUIHelpers;

	/* --------------- FACEBOOK --------------- */
	// Selector the email input of Facebook
	private By emailInputFB = By.cssSelector("#email");

	// Selector the password input of Facebook
	private By passwordInputFB = By.cssSelector("#pass");

	// Selector the button submit of Facebook
	private By submitBtnFB = By.cssSelector("#loginbutton");

	/* --------------- GOOGLE --------------- */
	// Selector the email input of Google
	private By emailInputGG = By.cssSelector("#identifierId");

	// Selector the next button in the step of entering email
	private By nextEmailBtn = By.cssSelector("#identifierNext");

	// Selector the password input of Google
	private By passwordInputGG = By.cssSelector("#password input");

	// Selector the next button in the step of entering password
	private By nextPasswordBtn = By.cssSelector("#passwordNext");

	public SocialNetworkPage(WebDriver driver) {
		this.driver = driver;
		validateUIHelpers = new ValidateUIHelpers(this.driver);
	}

	// Login account by Facebook account
	public HomePage authenticationWithFbAccount(String email, String password) {
		Set<String> windowId = driver.getWindowHandles(); // get window id of current window
		Iterator<String> itererator = windowId.iterator();
		String mainWinID = itererator.next();
		String newAdwinID = itererator.next();
		driver.switchTo().window(newAdwinID);
		// Input email and password to login FB
		validateUIHelpers.waitForPageLoaded();
		validateUIHelpers.setText(emailInputFB, email);
		validateUIHelpers.setText(passwordInputFB, password);
		validateUIHelpers.clickElement(submitBtnFB);
		driver.switchTo().window(mainWinID);

		return new HomePage(this.driver);
	}

	// Login account by Google account
	public HomePage authenticationWithGGAccount(String email, String password) {
		Set<String> windowId = driver.getWindowHandles(); // get window id of current window
		Iterator<String> itererator = windowId.iterator();
		String mainWinID = itererator.next();
		String newAdwinID = itererator.next();
		driver.switchTo().window(newAdwinID);
		// Input email and password to login FB
		validateUIHelpers.setText(emailInputGG, email);
		validateUIHelpers.clickElement(nextEmailBtn);
		validateUIHelpers.setText(passwordInputGG, password);
		validateUIHelpers.clickElement(nextPasswordBtn);
		driver.switchTo().window(mainWinID);

		return new HomePage(this.driver);
	}
}
